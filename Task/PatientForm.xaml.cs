﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Task.Models;

namespace Task
{
    /// <summary>
    /// Логика взаимодействия для PatientForm.xaml
    /// </summary>
    public partial class PatientForm : Window
    {
        private patient p;

        public PatientForm(patient p = null)
        {
            InitializeComponent();

            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            this.p = p;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (p.ID == 0)
            {
                id.Text = string.Empty;
                name.Text = string.Empty;
                r1.IsChecked = true;
                date.SelectedDate = DateTime.Now;
                address.Text = string.Empty;
                phone.Text = string.Empty;
            }
            else
            {
                id.Text = p.ID.ToString();
                name.Text = p.Name;
                if (p.Gender == "Мужской") r1.IsChecked = true; else r2.IsChecked = true;
                date.SelectedDate = p.Birthday;
                address.Text = p.address;
                phone.Text = p.phone;
            }
        }

        private void Otmena_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (id.Text != string.Empty) p.ID = int.Parse(id.Text);
            p.Name = name.Text;
            p.Gender = (r1.IsChecked == true) ? "Мужской" : "Женский";
            p.Birthday = date.SelectedDate.Value.Date;
            p.address = address.Text;
            p.phone = phone.Text;

            this.DialogResult = true;
            this.Close();
        }
    }
}
