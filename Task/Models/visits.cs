﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Task.Models
{
    public class visits
    {
        [Key]
        public int ID { get; set; }
        public DateTime date { get; set; }
        public string typeOfVisit { get; set; }
        public string diagnosis { get; set; }
        public int IDPatient { get; set; }
    }
}
