﻿using System.Collections.Generic;
using System.Linq;
using Task.Data;

namespace Task.Models
{
    public class patientCRUD
    {
        private AppDbContext context;

        public patientCRUD()
        {
            context = new AppDbContext();
        }

        //Get all
        public List<patient> GetAll()
        {
            var patientsList = context.patients.ToList();
            return patientsList;
        }

        //Insert
        public string Create(patient p)
        {
            context.patients.Add(p);
            context.SaveChanges();
            return "Save Successfully";
        }

        //Get by Id
        public patient GetById(int id)
        {
            patient p = context.patients.FirstOrDefault(s => s.ID == id);
            return p;
        }

        //Update
        public string Update(patient p)
        {
            context.patients.Update(p);
            context.SaveChanges();
            return "Update Successfully";
        }

        //Delete
        public string Delete(patient p)
        {
            context.Remove(p);
            context.SaveChanges();
            return "Delete Successfully";
        }
    }
}
