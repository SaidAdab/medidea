using System;
using System.ComponentModel.DataAnnotations;

namespace Task.Models
{
    public class patient
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public DateTime Birthday { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
    }
}
