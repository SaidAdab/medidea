﻿using System.Collections.Generic;
using Task.Data;
using System.Linq;

namespace Task.Models
{
    public class visitsCRUD
    {
        private AppDbContext context;

        public visitsCRUD()
        {
            context = new AppDbContext();
        }

        //Get all
        public List<visits> GetAll()
        {
            var patientsList = context.visits.ToList();
            return patientsList;
        }

        //Insert
        public string Create(visits v)
        {
            context.visits.Add(v);
            context.SaveChanges();
            return "Save Successfully";
        }

        //Get by Id
        public visits GetById(int id)
        {
            visits v = context.visits.FirstOrDefault(s => s.ID == id);
            return v;
        }

        //Get by Id Patient
        public List<visits> GetByIdPatient(int id)
        {
            var vList = context.visits.Where(s => s.IDPatient == id).ToList();
            return vList;
        }

        //Update
        public string Update(visits v)
        {
            context.visits.Update(v);
            context.SaveChanges();
            return "Update Successfully";
        }

        //Delete
        public string Delete(visits v)
        {
            context.Remove(v);
            context.SaveChanges();
            return "Delete Successfully";
        }

        //Delete By Patient Id
        public string DeleteByPatientId(int id)
        {
            var v = GetByIdPatient(id);

            context.RemoveRange(v);
            context.SaveChanges();
            return "Delete Successfully";
        }
    }
}
