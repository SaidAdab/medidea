﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Task.Models;

namespace Task
{
    /// <summary>
    /// Логика взаимодействия для visitsForm.xaml
    /// </summary>
    public partial class visitsForm : Window
    {
        private visits visit;
        private int _idPatient;

        public visitsForm(int idPatient, visits visit=null)
        {
            InitializeComponent();

            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            this.visit = visit;
            _idPatient = idPatient;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            idPatient.Text = _idPatient.ToString();

            if (visit.ID == 0)
            {
                id.Text = string.Empty;
                date.SelectedDate = DateTime.Now;
                type.SelectedValue = string.Empty;
                diagnosis.Text = string.Empty;
            }
            else
            {
                id.Text = visit.ID.ToString();
                date.SelectedDate = visit.date;
                type.SelectedIndex = (visit.typeOfVisit == "Первичный") ? 0 : 1;
                diagnosis.Text = visit.diagnosis;
            }
        }

        private void Otmena_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (id.Text != string.Empty) visit.ID = int.Parse(id.Text);
            visit.date = date.SelectedDate.Value.Date;
            visit.typeOfVisit = type.Text;
            visit.diagnosis = diagnosis.Text;
            visit.IDPatient = int.Parse(idPatient.Text);

            this.DialogResult = true;
            this.Close();
        }
    }
}
