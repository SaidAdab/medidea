using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Task.Data;
using Task.Models;

namespace Task
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private patientCRUD patient_crud;
        private visitsCRUD visits_crud;
        private bool loaded;

        public struct MyData
        {
            public int id { set; get; }
            public string title { set; get; }
            public int jobint { set; get; }
            public DateTime lastrun { set; get; }
            public DateTime nextrun { set; get; }
        }

        public MainWindow()
        {
            InitializeComponent();

            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            patient_crud = new patientCRUD();
            visits_crud = new visitsCRUD();
            loaded = false;

            DataGridTextColumn pcol1 = new DataGridTextColumn();
            DataGridTextColumn pcol2 = new DataGridTextColumn();
            DataGridTextColumn pcol3 = new DataGridTextColumn();
            DataGridTextColumn pcol4 = new DataGridTextColumn();
            DataGridTextColumn pcol5 = new DataGridTextColumn();
            DataGridTextColumn pcol6 = new DataGridTextColumn();

            PatientGrid.Columns.Add(pcol1);
            PatientGrid.Columns.Add(pcol2);
            PatientGrid.Columns.Add(pcol3);
            PatientGrid.Columns.Add(pcol4);
            PatientGrid.Columns.Add(pcol5);
            PatientGrid.Columns.Add(pcol6);
            pcol1.Binding = new Binding("ID");
            pcol2.Binding = new Binding("Name");
            pcol3.Binding = new Binding("Gender");
            pcol4.Binding = new Binding("Birthday");
            pcol5.Binding = new Binding("address");
            pcol6.Binding = new Binding("phone");
            pcol1.Header = "ID";
            pcol2.Header = "ФИО";
            pcol3.Header = "Пол";
            pcol4.Header = "Дата рождения";
            pcol5.Header = "Адрес";
            pcol6.Header = "Телефон";

            DataGridTextColumn vcol1 = new DataGridTextColumn();
            DataGridTextColumn vcol2 = new DataGridTextColumn();
            DataGridTextColumn vcol3 = new DataGridTextColumn();
            DataGridTextColumn vcol4 = new DataGridTextColumn();
            DataGridTextColumn vcol5 = new DataGridTextColumn();

            VisitsGrid.Columns.Add(vcol1);
            VisitsGrid.Columns.Add(vcol2);
            VisitsGrid.Columns.Add(vcol3);
            VisitsGrid.Columns.Add(vcol4);
            VisitsGrid.Columns.Add(vcol5);
            vcol1.Binding = new Binding("ID");
            vcol2.Binding = new Binding("date");
            vcol3.Binding = new Binding("typeOfVisit");
            vcol4.Binding = new Binding("diagnosis");
            vcol5.Binding = new Binding("IDPatient");
            vcol1.Header = "ID";
            vcol2.Header = "Дата посещения";
            vcol3.Header = "Тип посещения";
            vcol4.Header = "Диагноз";
            vcol5.Header = "IDPatient";

            vcol1.IsReadOnly = true;
            pcol1.IsReadOnly = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (var patient in patient_crud.GetAll())
                PatientGrid.Items.Add(patient);

            if (PatientGrid.Items.Count != 0)
            {
                PatientGrid.SelectedIndex = 0;

                var p = (patient)PatientGrid.SelectedItem;

                VisitsGrid.Items.Clear();

                foreach (var visit in visits_crud.GetByIdPatient(p.ID))
                    VisitsGrid.Items.Add(visit);
            }
            loaded = true;
        }

        private void vAdd_Click(object sender, RoutedEventArgs e)
        {
            if (PatientGrid.SelectedIndex !=-1)
            {
                var idContent = PatientGrid.Columns[0].GetCellContent(PatientGrid.SelectedItem) as TextBlock;
                visits v = new visits();

                visitsForm frm = new visitsForm(int.Parse(idContent.Text), v);
                frm.ShowDialog();

                if (frm.DialogResult == true)
                    visits_crud.Create(v);

                VisitsGrid.Items.Clear();

                foreach (var visit in visits_crud.GetByIdPatient(v.IDPatient))
                    VisitsGrid.Items.Add(visit);
            }
        }

        private void vEdit_Click(object sender, RoutedEventArgs e)
        {
            if (VisitsGrid.SelectedIndex != -1)
            {
                visits v = (visits)VisitsGrid.SelectedItem;

                visitsForm frm = new visitsForm(v.IDPatient, v);
                frm.ShowDialog();

                if (frm.DialogResult == true)
                    visits_crud.Update(v);

                VisitsGrid.Items.Clear();

                foreach (var visit in visits_crud.GetByIdPatient(v.IDPatient))
                    VisitsGrid.Items.Add(visit);
            }
        }

        private void vDel_Click(object sender, RoutedEventArgs e)
        {
            if (VisitsGrid.SelectedIndex != -1)
            {
                visits v = (visits)VisitsGrid.SelectedItem;

                MessageBoxResult res = MessageBox.Show("Вы уверены?", "Удаление", MessageBoxButton.YesNo);
                if (res == MessageBoxResult.Yes)
                    visits_crud.Delete(v);

                VisitsGrid.Items.Clear();

                foreach (var visit in visits_crud.GetByIdPatient(v.IDPatient))
                    VisitsGrid.Items.Add(visit);
            }
        }

        private void PatientGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PatientGrid.Items.Count != 0 && PatientGrid.SelectedIndex != -1)
            {
                var p = (patient)PatientGrid.SelectedItem;

                VisitsGrid.Items.Clear();

                foreach (var visit in visits_crud.GetByIdPatient(p.ID))
                    VisitsGrid.Items.Add(visit);
            }

            loaded = false;
        }

        private void pAdd_Click(object sender, RoutedEventArgs e)
        {
            if (PatientGrid.SelectedIndex != -1)
            {
                patient p = new patient();

                PatientForm frm = new PatientForm(p);
                frm.ShowDialog();

                if (frm.DialogResult == true)
                    patient_crud.Create(p);

                PatientGrid.Items.Clear();

                foreach (var patient in patient_crud.GetAll())
                    PatientGrid.Items.Add(patient);

                VisitsGrid.Items.Clear();

                if (PatientGrid.Items.Count != 0)
                    foreach (var visit in visits_crud.GetByIdPatient(p.ID))
                        VisitsGrid.Items.Add(visit);
            }
        }

        private void pEdit_Click(object sender, RoutedEventArgs e)
        {
            if (PatientGrid.SelectedIndex != -1)
            {
                patient p = (patient)PatientGrid.SelectedItem;

                PatientForm frm = new PatientForm(p);
                frm.ShowDialog();

                if (frm.DialogResult == true)
                {
                    patient_crud.Update(p);

                    PatientGrid.Items.Clear();

                    foreach (var patient in patient_crud.GetAll())
                        PatientGrid.Items.Add(patient);

                    if (PatientGrid.Items.Count != 0)
                        foreach (var visit in visits_crud.GetByIdPatient(p.ID))
                            VisitsGrid.Items.Add(visit);
                }
            }
        }

        private void pDel_Click(object sender, RoutedEventArgs e)
        {
            if (PatientGrid.SelectedIndex != -1)
            {
                patient p = (patient)PatientGrid.SelectedItem;

                MessageBoxResult res = MessageBox.Show("Вы уверены?", "Удаление", MessageBoxButton.YesNo);

                if (res == MessageBoxResult.Yes)
                {
                    visits_crud.DeleteByPatientId(p.ID);

                    patient_crud.Delete(p);
                }

                PatientGrid.Items.Clear();

                foreach (var patient in patient_crud.GetAll())
                    PatientGrid.Items.Add(patient);

                if (PatientGrid.Items.Count != 0)
                {
                    PatientGrid.SelectedIndex = 0;

                    var p1 = (patient)PatientGrid.SelectedItem;

                    foreach (var visit in visits_crud.GetByIdPatient(p1.ID))
                        VisitsGrid.Items.Add(visit);
                }
            }
        }
    }
}
